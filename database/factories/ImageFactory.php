<?php

namespace Database\Factories;

use App\Models\Image;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;

class ImageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Image::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'img' => $this->getImage(rand(1, 8)),
        ];
    }

    private function getImage($img_number = 1):string
    {
        $path = storage_path() . "/seed_img/" . $img_number . ".jpg";
        $img_name = md5($path) . ".jpg";
        $resize = \Intervention\Image\Facades\Image::make($path)->encode('jpg');
        Storage::disk('public')->put('img/' . $img_name, $resize->__toString());

        return 'img/' . $img_name;
    }
}
