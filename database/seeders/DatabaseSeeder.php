<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Image;
use App\Models\Like;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(20)->create();
        for ($i = 0; $i < 20; $i++) {
            $user = User::find(rand(1, 20));
            $user->followers()->attach(User::find(rand(1, 20)));

            $img = Image::factory()->state([
                'user_id' => rand(1, 20)
            ])->create();

            $like = Like::factory()->state([
                'user_id' => $user,
                'image_id' => $img
            ])->create();

            Comment::factory(rand(3, 8))->state([
                'user_id' => rand(1, 20),
                'image_id' => $img
            ])->create();
        }

    }
}
