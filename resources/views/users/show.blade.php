@extends('layouts.app')

@section('content')

    <div class="card text-center mb-4">
        <div class="card-body">
            <h5 class="card-title">{{$user->name}}</h5>
            <div class="row">
                <div class="col-sm-6">
                    <div class="card">
                        <a href="" data-toggle="collapse" data-target="#collapseFollowers"
                           aria-expanded="false" aria-controls="collapseFollowers">
                            <div class="card-body">
                                <h5 class="card-title">Followers</h5>
                                <p class="card-text">{{$user->followers->count()}}</p>
                            </div>
                        </a>
                        <div class="collapse" id="collapseFollowers">
                            @foreach($user->followers as $val)
                                <ul class="m-0 text-center">
                                    <li>
                                        {{$val->name}}
                                    </li>
                                </ul>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="card">
                        <a href="" data-toggle="collapse" data-target="#collapseFollowings"
                           aria-expanded="false" aria-controls="collapseFollowings">
                            <div class="card-body">
                                <h5 class="card-title">Followings</h5>
                                <p class="card-text">{{$user->followings->count()}}</p>
                            </div>
                        </a>
                        <div class="collapse" id="collapseFollowings">
                            @foreach($user->followings as $val)
                                <ul class="m-0 text-center">
                                    <li>
                                        {{$val->name}}
                                    </li>
                                </ul>
                            @endforeach
                        </div>
                    </div>
                </div>


            </div>
            <button class="btn btn-primary mt-3" type="button" data-toggle="collapse" data-target="#collapseExample"
                    aria-expanded="false" aria-controls="collapseExample">
                Add New Photo
            </button>

        </div>
        <div class="collapse" id="collapseExample">
            <form enctype="multipart/form-data" method="POST" action="{{route('image.store')}}">
                @csrf
                <div class="form-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="customFile" name="img">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Add</button>
            </form>
        </div>
    </div>

    @if($user->images->isNotEmpty())
        <div class="row row-cols-1 row-cols-md-3">
            @foreach($user->images as $img)
                <div class="col mb-4">
                    <div class="card h-100">
                        <img src="{{ asset('/storage/' . $img->img) }}" class="card-img-top" alt="{{$img->img}}">
                        <div class="card-body">
                            <p class="card-text"><span>Like: {{ $img->likes->count() }}</span></p>
                        </div>
                        <div class="card-footer">
                            <form action="{{ route('images.destroy', ['img' => $img]) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endif


@endsection
