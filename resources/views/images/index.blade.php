@extends('layouts.app')

@section('content')

    @foreach($images as $img)
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="card-title">{{ $img->user->name }}</h5>
            </div>
            <img src="{{asset('/storage/' . $img->img)}}" class="card-img-top" alt="$img->img">
            <div class="card-body">
                <p class="card-text"><span>Like: {{ $img->likes->count() }}</span></p>
                <div class="row mb-2">
                    @if (!$img->likes->contains('user_id', Auth::user()->id))
                        <form action="{{ route('likes.store', ['img' => $img]) }}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-outline-danger">Like</button>
                        </form>
                    @else
                        <form action="{{ route('likes.destroy', ['img' => $img]) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Like</button>
                        </form>
                    @endif

                    @if(Auth::user()->followings->contains('id', $img->user->id))
                            <form action="{{ route('users.unfollow', ['user' => $img->user]) }}" method="POST">
                                @csrf
                                <button type="submit" class="btn btn-primary">Unfollow</button>
                            </form>
                    @else
                            <form action="{{ route('users.follow', ['user' => $img->user]) }}" method="POST">
                                @csrf
                                <button type="submit" class="btn btn-outline-primary">Follow</button>
                            </form>
                    @endif

                        <p>
                            <button class="btn btn-primary" type="button" data-toggle="collapse"
                                    data-target="#collapseExample{{$img->id}}" aria-expanded="false"
                                    aria-controls="collapseExample">
                                Show comments
                            </button>
                        </p>

                </div>

                <div class="collapse" id="collapseExample{{$img->id}}">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$img->id}}">
                        Add Comment
                    </button>
                    @foreach($img->comments as $comment)
                        <div class="card card-body mb-3">
                            <span><b>{{$comment->user->name}}</b></span>
                            {{$comment->body}}
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="modal fade" id="exampleModal{{$img->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Adding Comment</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{ route('comments.store', ['img' => $img]) }}" method="POST">
                        <div class="modal-body">
                            @csrf
                            <textarea class="form-control" placeholder="Add Comment" name="body" rows="3">{{old('body')}}</textarea>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach


@endsection
