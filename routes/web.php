<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [\App\Http\Controllers\ImageController::class, 'index'])
    ->middleware('auth');

Route::post('/', [\App\Http\Controllers\LikesController::class, 'store'])
    ->name('likes.store')
    ->middleware('auth');

Route::delete('/destroy{img}', [\App\Http\Controllers\LikesController::class, 'destroy'])
    ->name('likes.destroy')
    ->middleware('auth');

Route::post('/follow{user}', [\App\Http\Controllers\UsersController::class, 'follow'])
    ->name('users.follow')
    ->middleware('auth');

Route::post('/unfollow{user}', [\App\Http\Controllers\UsersController::class, 'unfollow'])
    ->name('users.unfollow')
    ->middleware('auth');

Route::post('/comment', [\App\Http\Controllers\CommentsController::class, 'store'])
    ->name('comments.store')
    ->middleware('auth');

Route::get('/show', [\App\Http\Controllers\UsersController::class, 'show'])
    ->name('user.show')
    ->middleware('auth');

Route::post('/image/store', [\App\Http\Controllers\ImageController::class, 'store'])
    ->name('image.store')
    ->middleware('auth');

Route::delete('/images/destroy{img}', [\App\Http\Controllers\ImageController::class, 'destroy'])
    ->name('images.destroy')
    ->middleware('auth');

Auth::routes();

