<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'body' => 'required|min:2|max:292',
        ]);
        $comment = new Comment();
        $comment->body = $request->input('body');
        $comment->user_id = $request->user()->id;
        $comment->image_id = $request->get('img');
        $comment->save();

        return redirect()->back();
    }
}
