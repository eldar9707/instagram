<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    /**
     * @param User $user
     * @return RedirectResponse
     */
    public function follow(User $user): RedirectResponse
    {
        Auth::user()->followings()->attach($user);

        return redirect()->back();
    }

    /**
     * @param User $user
     * @return RedirectResponse
     */
    public function unfollow(User $user): RedirectResponse
    {
        Auth::user()->followings()->detach($user);
        return redirect()->back();
    }

    /**
     * @return Application|Factory|View
     */
    public function show()
    {
        $user = Auth::user();
        return view('users.show', compact('user'));
    }
}
