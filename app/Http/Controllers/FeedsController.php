<?php

namespace App\Http\Controllers;

use App\Models\Image;
use Illuminate\Http\Request;

class FeedsController extends Controller
{
    public function index()
    {
        $images = Image::all();
        return view('feeds.index', compact($images));
    }
}
