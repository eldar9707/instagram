<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Like;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $images = Image::all();
        return view('images.index', compact('images'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'img' => 'required|image'
        ]);
        $data = $request->all();
        $file = $request->file('img');
        if (!is_null($file)) {
            $path = $file->store('img', 'public');
            $data['img'] = $path;
        }
        $data['user_id'] = $request->user()->id;
        Image::create($data);

        return redirect()->back();
    }

    /**
     * @param Image $img
     * @return RedirectResponse
     */
    public function destroy(Image $img): RedirectResponse
    {
        $img->delete();

        return redirect()->back();
    }
}
