<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Like;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikesController extends Controller
{
    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $like = new Like();
        $like->user()->associate($request->user());
        $like->image()->associate($request->get('img'));
        $like->save();

        return redirect()->back();
    }

    /**
     * @param Image $img
     * @return RedirectResponse
     */
    public function destroy(Image $img): RedirectResponse
    {
        foreach ($img->likes as $like) {
            if ($like->image_id == $img->id and $like->user_id == Auth::user()->id) {
                $like->delete();
            }
        }

        return redirect()->back();
    }
}
